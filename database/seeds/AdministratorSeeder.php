<?php

use Illuminate\Database\Seeder;

class AdministratorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrator = new \App\User;
        $administrator->username = "aprillisda";
        $administrator->name = "Administrator : Aprillisda";
        $administrator->email = "aprillisda@gmail.com";
        $administrator->roles = json_encode(["ADMIN"]);
        $administrator->password = \Hash::make("annisa");
        $administrator->avatar = "none.png";
        $administrator->phone = "082115499410";
        $administrator->address = "Gunung Sari, Imbanagara Kecamatan Ciamis Kabupaten Ciamis";
        $administrator->save();
        $this->command->info("User administrator berhasil dibuat!");
    }
}
